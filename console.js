const repl = require('repl')
const app = require('./server')
const fetch = require('node-fetch')
const questionsController = require('./server/controllers').questions
const Question = require('./server/models').question

const replServer = repl.start({
  prompt: 'question_game > ',
})
replServer.context.questionsController = questionsController
replServer.context.Question = Question
replServer.context.fetch = fetch
replServer.context.app = app
