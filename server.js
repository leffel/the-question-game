const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')

const app = express()
app.set('port', process.env.PORT || 3001)
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

require('./server/routes')(app)
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'))
}

app.listen(app.get('port'), () => {
  console.log(`server at: http://localhost:${app.get('port')}/`)
})

module.exports = app
