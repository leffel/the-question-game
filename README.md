# The Question Game  

**[Play it here.](https://the-question-game.herokuapp.com/)**

The Question Game is a collaborative party game for groups of folx who want to know each other better. 

Only one person needs to run the app at a time. It's easy to pick up, put down, and add more players as you go.

## Running TQG locally  

First, make sure you have [PostgreSQL](https://www.postgresql.org/) installed.

```brew install postgresql```

Next, clone the repo and install dependencies.

```
git clone git@gitlab.com:leffel/the-question-game.git
cd the-question-game
npm install
```

Then, run migrations and seed the database with some random questions.
```
sequelize db:migrate
sequelize db:seed:all
```

Finally, start the client-live-reloading server and the talking-to-the-database server concurrently.
```
npm run dev
```

And if you'd like to build the frontend app, be sure to do it from within the client directory.
```
cd client/
npm build
```

Enjoy! 