import styled from 'styled-components'

const ToggleSpan = styled.span`
  color: #03A383;
  font-size: 1.2em;
  &:after {
    display: inline-block;
    content: '⌃';
    transform: ${props => props.open ? 'rotate(180deg)' : 'rotate(90deg)' };
    margin: ${props => props.open ? '0 0 0 0.38em' : '0 0 0 0.2em' };
  }
`
export default ToggleSpan
