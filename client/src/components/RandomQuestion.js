import React, { Component } from 'react'
import styled from 'styled-components'
import ToggleSpan from './ToggleSpan'

class RandomQuestion extends Component {
  constructor(props) {
    super(props)
    this.state = {
      questionText: null,
      questionLoaded: false,
      open: false,
      hide: true
    }
    this.getRandomQuestion = this.getRandomQuestion.bind(this)
  }

  getRandomQuestion() {
    fetch('/api/questions/random')
    .then(res => res.json())
    .then(question => this.setState({ questionText: question.text, questionLoaded: true }))
  }

  render() {
    return (
      <QuestionContainer>
        <ToggleSpan open={this.state.open} onClick={this.props.toggleDrawer.bind(this)}>
          Help, I need a question!
        </ToggleSpan>
        <QuestionControls hide={this.state.hide}>
          <p>Asking your own question is the most fun way to play.</p>
          <p>{"But if you're really stuck..." }</p>
          <Button onClick={this.getRandomQuestion}> get random question </Button>
          <QuestionDisplay hide={!this.state.questionLoaded}>
            <p>{this.state.questionText}</p>
          </QuestionDisplay>
        </QuestionControls>
      </QuestionContainer>
    )
  }
}

export default RandomQuestion

const QuestionContainer = styled.div`
  margin: 1.6em auto;
`

const QuestionControls = styled.div`
  display: ${props => props.hide ? 'none' : 'block'}
`

const QuestionDisplay = styled.div`
  background-color: #dfe;
  font-size: 1.2em;
  padding: 0.6em;
  display: ${props => props.hide ? 'none' : 'block'}
`

const Button = styled.button`
  display: block;
  border: solid #03A383 1px;
  font-size: 1.4em;
  height: 2.6em;
  color: #03A383;
  width: 100%;
  margin: 0.8em 0;
  background-color: #fff;
`
