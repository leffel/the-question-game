import React, { Component } from 'react'
import styled from 'styled-components'
import RandomQuestion from './RandomQuestion.js'

class Instructions extends Component {
  render() {
    if (this.props.display && this.props.asking){
      return (
        <div>
          <ActingPlayer>Player #{this.props.askingPlayer}</ActingPlayer>
          <p>Think of a question that anyone in the group can answer.</p>
          <p>Then, ask it aloud.</p>
          <Button onClick={this.props.getAnswer}> next </Button>
          <RandomQuestion toggleDrawer={this.props.toggleDrawer}></RandomQuestion>
        </div>
      )
    } else if (this.props.display && !this.props.asking) {
      return (
        <div>
          <ActingPlayer>Player #{this.props.answeringPlayer}</ActingPlayer>
          <p>Please answer Player #{this.props.askingPlayer + "'s question"}.</p>
          <p>{"You can pass if you'd prefer not to answer."}</p>
          <Button onClick={this.props.newQuestion}> next </Button>
          <PassButton onClick={this.props.getAnswer}> pass </PassButton>
        </div>
      )
    } else {
      return null
    }
  }
}

export default Instructions

const ActingPlayer = styled.h2`
  font-weight: bold;
  font-size: 1.4em;
  background-color: #dfe;
  padding: 0.6em;
  border: 1px solid #ded;
`

const Button = styled.button`
  font-size: 1.6em;
  height: 2.2em;
  width: 100%;
  background-color: #03A383;
  color: #fff;
`

const PassButton = Button.extend`
  border: solid #03A383 1px;
  color: #03A383;
  background-color: #fff;
  margin: 0.5em 0 1em 0;
`
