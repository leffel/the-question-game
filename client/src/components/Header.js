import React from 'react'
import styled from 'styled-components'

class Header extends React.Component {
  render() {
    if (this.props.minimized){
      return (
        <div>
          <Title>the Question Game 💁‍♀️💬</Title>
        </div>
      )
    } else {
      return (
        <div>
          <Title>the Question Game 💁‍♀️💬</Title>
          <p>This is a party game for people who want to get to know each other better.</p>
          <p>Comfortably arrange yourselves so that all players can see each other.</p>
          <p>Play for as long as you like and no longer. :)</p>
        </div>
      )
    }
  }
}

export default Header

const Title = styled.h1`
  font-family: 'Helvetica';
  font-size: 1.3em;
  font-weight: 600;
  color: #03A383;
`
