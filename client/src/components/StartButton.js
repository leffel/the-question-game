import React, { Component } from 'react'
import styled from 'styled-components'

class StartButton extends Component {
  render() {
    if (this.props.display){
      return (
        <div>
          <Button onClick={this.props.startGame}> start </Button>
        </div>
      )
    } else {
      return null
    }
  }
}

export default StartButton

const Button = styled.button`
  border: solid #5DFDCB 2px;
  background-color: #03A383;
  font-size: 1.6em;
  height: 2.2em;
  width: 100%;
  color: #fff;
`
