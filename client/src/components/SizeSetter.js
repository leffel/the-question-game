import React, { Component } from 'react'
import styled from 'styled-components'
import ToggleSpan from './ToggleSpan'

class SizeSetter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      hide: true
    }
  }

  render() {
    if (this.props.minimizeControls) {
      return (
        <div>
          <ToggleSpan open={this.state.open} onClick={this.props.toggleDrawer.bind(this)}>
            Add or remove players.
          </ToggleSpan>
          <SizeControls hide={this.state.hide}>
            <p>How many people are playing?</p>
            <Button onClick={this.props.partyDecrementer}> - </Button>
            <PartySize> { this.props.partySize } </PartySize>
            <Button onClick={this.props.partyIncrementer}> + </Button>
          </SizeControls>
          <NumberPlayers hide={!this.state.hide}>
            <p>There are {this.props.partySize} people playing.</p>
          </NumberPlayers>
        </div>
      )
    } else {
      return (
        <div>
          <SizeControls>
            <p>How many people are playing?</p>
            <Button onClick={this.props.partyDecrementer}> - </Button>
            <PartySize> { this.props.partySize } </PartySize>
            <Button onClick={this.props.partyIncrementer}> + </Button>
          </SizeControls>
        </div>
      )
    }
  }
}

export default SizeSetter

const SizeControls = styled.div`
  display: ${props => props.hide ? 'none' : 'block'}
`

const NumberPlayers = styled.div`
  display: ${props => props.hide ? 'none' : 'block'}
`

const Button = styled.button`
  border-radius: 30%;
  vertical-align: text-bottom;
  border: solid #03A383 2px;
  font-size: 1.6em;
  height: 2.2em;
  width: 2.2em;
  color: #666;
  background-color: #fff;
`

const PartySize = styled.span`
  font-size: 4.2em;
`
