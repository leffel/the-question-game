import React, { Component } from 'react'
import styled from 'styled-components'
import Header from './components/Header'
import SizeSetter from './components/SizeSetter'
import StartButton from './components/StartButton'
import Instructions from './components/Instructions'

class Game extends Component {
  constructor(props) {
    super(props)
    this.state = {
      gameStarted: false,
      minimizeSizeSetter: false,
      partySize: 4,
      asking: false,
      askingPlayer: null,
      answeringPlayer: null
    }
    this.startGame = this.startGame.bind(this)
    this.partyUpdater = this.partyUpdater.bind(this)
    this.getAnswer = this.getAnswer.bind(this)
    this.newQuestion = this.newQuestion.bind(this)
    this.randomPlayer = this.randomPlayer.bind(this)
  }

  startGame() {
    this.setState({
      gameStarted: true,
      minimizeSizeSetter: true
    })
    this.newQuestion()
  }

  partyUpdater(mode) {
    let size = this.state.partySize
    if (mode.inc) {
      size++
    } else if (size > 2) {
      size--
    }
    this.setState({ partySize: size })
  }

  getAnswer() {
    this.setState({
      asking: false,
      answeringPlayer: this.randomPlayer(),
      minimizeSizeSetter: true
    })
  }

  newQuestion() {
    this.setState({
      asking: true,
      askingPlayer: this.state.answeringPlayer || this.randomPlayer(),
      minimizeSizeSetter: true
    })
  }

  randomPlayer() {
    let rand = this.state.answeringPlayer
    while (rand === this.state.answeringPlayer) {
      rand = (Math.floor(Math.random() * this.state.partySize)) + 1
    }
    return rand
  }

  toggleDrawer() {
    this.setState({
      open: !(this.state.open),
      hide: !(this.state.hide)
    })
  }

  render() {
    return (
      <GameWrapper>
        <Header       minimized={this.state.gameStarted} />
        <StartButton  display={!this.state.gameStarted}
                      startGame={this.startGame} />
        <Instructions display={this.state.gameStarted}
                      asking={this.state.asking}
                      askingPlayer={this.state.askingPlayer}
                      answeringPlayer={this.state.answeringPlayer}
                      getAnswer={this.getAnswer}
                      newQuestion={this.newQuestion}
                      toggleDrawer={this.toggleDrawer} />
        <SizeSetter   minimizeControls={this.state.gameStarted}
                      partySize={this.state.partySize}
                      partyIncrementer={() => this.partyUpdater({ inc: true })}
                      partyDecrementer={() => this.partyUpdater({ inc: false })}
                      toggleDrawer={this.toggleDrawer} />
      </GameWrapper>
    )
  }
}

export default Game

const GameWrapper = styled.div`
  font-family: 'Helvetica';
  font-size: 1em;
  margin: 2em auto;
  width: 17.8em;
  text-align: center;
  p {
    font-size: 1.2em;
  }
`
