const Question = require('../models').question

module.exports = {
  getRandom(req, res) {
    return Question
      .findAndCountAll()
      .then(questionsWithCount => {
        let { count, rows } = questionsWithCount
        let rand = Math.floor(Math.random() * count)
        let question = rows[rand].dataValues
        return question
      })
      .then(question => res.status(200).send(question))
      .catch(error => res.status(400).send(error))
  }
}
