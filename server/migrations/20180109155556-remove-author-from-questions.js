'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('questions', 'author');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('questions', 'author', { type: Sequelize.STRING });
  }
};
