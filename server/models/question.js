'use strict'
module.exports = (sequelize, DataTypes) => {
  var Question = sequelize.define('question', {
    text: {
      type: DataTypes.STRING,
      allowNull: false
    }
  })
  return Question
}
