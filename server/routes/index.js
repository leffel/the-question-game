const questionsController = require('../controllers').questions

module.exports = (app) => {
  app.get('/api/questions/random', questionsController.getRandom)
}
