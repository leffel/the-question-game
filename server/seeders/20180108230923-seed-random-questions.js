'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let now = new Date()

    return queryInterface.bulkInsert('questions', [
      {
        text: "if you had to construct your perfect significant other from the body parts of the other players, what would you choose?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "assuming there was an outpost set up there, would you take a one-way trip to Mars?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what are you most proud of that you've done in the last year?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "do you believe in reincarnation?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "if you had to have NO hair or 4x the amount of hair, body hair, head hair, everything included... which would you choose?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what would you do with mind control powers?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what do you think human life would be like post-singularity (when humans can download their brains/conscious minds into computers)?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "do you think people are irreplaceable?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "do you think some people are better than others?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what superpower would you have if you could choose?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "if you had a superpower, would you keep it secret?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "if you could know how or when you were going to die, which would you choose?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "is there anything you wish you could have told someone that now you can't?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what is a place you'd like to visit that you know nothing about?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "how would you hide if you had to be in the witness protection program?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what's the most scared you have ever been?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "what's something you're excited about right now?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "if you could travel to a time in the past or in the future, which would you choose and why?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "how do you think the world will end?",
        createdAt: now,
        updatedAt: now
      },
      {
        text: "do you think you've met 'the one'?",
        createdAt: now,
        updatedAt: now
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('questions', null, {});
  }
};
